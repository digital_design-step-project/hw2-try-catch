const books = [
    { 
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70 
    }, 
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    }, 
    { 
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    }, 
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];
const divElement = document.getElementById("root");
let invalidCount = 0;

function allBooks(params) {
    const ul = document.createElement("ul");

    params.forEach(book => {
        try {
            if (!book.author || !book.name || !book.price) {
                throw new Error("Invalid value");
            }

            const li = document.createElement("li");

            li.textContent = `${book.author} — ${book.name} — $${book.price}`;
            ul.append(li);
        } catch (error) {
            console.log(error.message);
            invalidCount++;
        }
    });

    divElement.append(ul);

    if (invalidCount > 0) {
        console.log(`Total invalid values: ${invalidCount}`);
    }
}

allBooks(books);